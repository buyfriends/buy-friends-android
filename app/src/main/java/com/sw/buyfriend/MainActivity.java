package com.sw.buyfriend;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.sw.buyfriend.ui.adapter.NavDrawerItem;
import com.sw.buyfriend.ui.fragment.AlternativeFragment;
import com.sw.buyfriend.ui.fragment.MapFragment;
import com.sw.buyfriend.ui.fragment.ProductFragment;
import com.sw.buyfriend.ui.fragment.ScannerFragment;

import java.util.ArrayList;


public class MainActivity extends AbstractActivity {

    @Override
    public void prepareListData() {
        mNavDrawerItems = new ArrayList<>();
        mNavDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.app_name), R.drawable.ic_account_header) {
            @Override
            public void onClick() {
//                changeFragment();
            }
        });
        mNavDrawerItems.add(new NavDrawerItem("Scan produit", R.drawable.ic_action_settings) {
            @Override
            public void onClick() {
               changeFragment(new ScannerFragment());
            }
        });
        mNavDrawerItems.add(new NavDrawerItem(("Fiche produit"), R.drawable.ic_action_settings) {
            @Override
            public void onClick() {
                changeFragment(new ProductFragment());
            }
        });

        mNavDrawerItems.add(new NavDrawerItem(("Soutions alternatives"), R.drawable.ic_action_settings) {
            @Override
            public void onClick() {
                changeFragment(new AlternativeFragment());
            }
        });

        mNavDrawerItems.add(new NavDrawerItem(("Autour de moi"), R.drawable.ic_action_settings) {
            @Override
            public void onClick() {
                changeFragment(new MapFragment());
            }
        });
    }


    public void openBrandDetails(String productCode){
        ProductFragment fragment = new ProductFragment();
        fragment.setProductCode(productCode);
        changeFragment(fragment);

    }

    public void openAlternatives(){
        AlternativeFragment fragment = new AlternativeFragment();
        changeFragment(fragment);
    }

    public void openMap(){
        MapFragment fragment = new MapFragment();
        changeFragment(fragment);
    }

    @Override
    public void initFragment() {
        openAlternatives();
        //changeFragment(new ScannerFragment());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ((TextView) findViewById(R.id.tv_dl_username)).setText("Cityzen eyes !");
        ((TextView) findViewById(R.id.tv_dl_secondary)).setText("demo@cityzenproject.fr");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
