package com.sw.buyfriend.pojo;

import java.util.ArrayList;

/**
 * Created by yamzi on 09/05/2015.
 */
public class Shop {

    public String name;
    public String adress;
    public int id;
    public int thumbnail;
    public ArrayList<Integer> categories;



    public Shop(String _name,String _adress,int _id,int _thumbnail){
        name = _name;
        adress =_adress;
        id = _id;
        thumbnail = _thumbnail;

        categories = new ArrayList<>();
    }

    public static ArrayList<Shop> getShopsByCategory(ArrayList<Shop> shops, int cat){

        ArrayList<Shop> alternativeShops = new ArrayList<>();

        for (int i=0;i<shops.size();i++){
            if(shops.get(i).categories.contains(cat)){
                alternativeShops.add(shops.get(i));
            }
        }

        return alternativeShops;

    }

}
