package com.sw.buyfriend.pojo;

public class Product {

    private String _title;
    private Integer _category;
    private String _brand;
    public String code;

    public Product(String t, Integer cat, String brd) {
        _title = t;
        _category = cat;
        _brand = brd;

    }


    public static Product buildProductFromProductCode(String code) {
        if (code.equals("20147426")) {
            Product prd =  new Product("Ice Tea", 1, null);
            prd.code = code;
            return prd;
        } else {
            return null;
        }
    }

}
