package com.sw.buyfriend.pojo;

public class Note {

    private String sujet;
    private int image;

    public Note(String sujet, int image) {
        this.sujet = sujet;
        this.image = image;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
