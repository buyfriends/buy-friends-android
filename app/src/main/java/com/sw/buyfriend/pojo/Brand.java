package com.sw.buyfriend.pojo;

import com.sw.buyfriend.R;

import java.util.ArrayList;
import java.util.List;

public class Brand {

    public String name;
    public int thumbNail;

    // Array List with: total, detailled scores list
    private List<Integer> criteria_env = new ArrayList<>();
    private List<Integer> criteria_social = new ArrayList<>();
    private List<Integer> criteria_health = new ArrayList<>();
    private List<Note> note;

    public Brand(String name, int thumbNail, List<Note> note) {
        this.name = name;
        this.thumbNail = thumbNail;
        this.note = note;
    }

    public List<Note> getNote() {
        return note;
    }

    public void setNote(List<Note> note) {
        this.note = note;
    }

    public List<Integer> getCriteria_health() {
        return criteria_health;
    }

    public void setCriteria_health(List<Integer> criteria_health) {
        this.criteria_health = criteria_health;
    }

    public List<Integer> getCriteria_social() {
        return criteria_social;
    }

    public Number getAverage() {
        return 0.0+(criteria_social.get(0)+criteria_health.get(0)+criteria_env.get(0))/(criteria_social.size()+criteria_health.size()+criteria_env.size());

    }

    public void setCriteria_social(List<Integer> criteria_social) {
        this.criteria_social = criteria_social;
    }

    public List<Integer> getCriteria_env() {
        return criteria_env;
    }

    public void setCriteria_env(List<Integer> criteria_env) {
        this.criteria_env = criteria_env;
    }

    public int getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(int thumbNail) {
        this.thumbNail = thumbNail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brand(String _name, int _thumb) {
        name = _name;
        thumbNail = _thumb;
    }

    public void pushEnvScore(Integer score) {
        criteria_env.set(0, criteria_env.get(0) + score);
        criteria_env.add(score);
    }

    public void pushSocScore(Integer score) {
        criteria_social.set(0, criteria_social.get(0) + score);
        criteria_social.add(score);
    }

    public void pushHeaScore(Integer score) {
        criteria_health.set(0, criteria_health.get(0) + score);
        criteria_health.add(score);
    }

    public static Brand buildBrandFromProductCode(String code) {
        if (code.equals("20147426")) {
            Brand brand = new Brand("Nestle", R.drawable.logo_nestle);
            List<Note> notes = new ArrayList<>();
            notes.add(new Note("Social", R.drawable.societe));
            notes.add(new Note("Environnement", R.drawable.environnement));
            notes.add(new Note("Sante", R.drawable.sante));
            brand.setNote(notes);
            return brand;
        } else {
            return null;
        }
    }

}
