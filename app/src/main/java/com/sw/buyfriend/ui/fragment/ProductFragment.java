package com.sw.buyfriend.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sw.buyfriend.MainActivity;
import com.sw.buyfriend.R;
import com.sw.buyfriend.pojo.Brand;
import com.sw.buyfriend.pojo.Product;
import com.sw.buyfriend.ui.adapter.NoteAdapter;


public class ProductFragment extends Fragment {

    private Product currentProduct;
    private Brand currentBrand;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        TextView tv = (TextView) getActivity().findViewById(R.id.marqueTitle);
        tv.setText(currentBrand.name);


        ImageView logo = (ImageView) getActivity().findViewById(R.id.marqueThumb);
        logo.setImageResource(currentBrand.thumbNail);
        ListView listView = (ListView) getActivity().findViewById(R.id.alternativeList);

        TextView score = (TextView) getActivity().findViewById(R.id.marqueScore);
        tv.setText(String.valueOf(currentBrand.getAverage()));

        NoteAdapter noteAdapter = new NoteAdapter(getActivity(), currentBrand.getNote());
        listView.setAdapter(noteAdapter);


        Button button = (Button) getActivity().findViewById(R.id.dislikeBtn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                ((MainActivity) v.getContext()).openAlternatives();
            }
        });
        return inflater.inflate(R.layout.activity_details_marques, container, false);
    }

    public void onResume() {
        super.onResume();
    }

    public void setProductCode(String productCode){
        productCode = "20147426";
        currentProduct = Product.buildProductFromProductCode(productCode);
        currentBrand = Brand.buildBrandFromProductCode(productCode);

    }
}
