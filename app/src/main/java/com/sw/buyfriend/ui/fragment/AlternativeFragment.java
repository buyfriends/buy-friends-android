package com.sw.buyfriend.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.sw.buyfriend.MainActivity;
import com.sw.buyfriend.R;
import com.sw.buyfriend.pojo.Brand;
import com.sw.buyfriend.pojo.Product;
import com.sw.buyfriend.pojo.Shop;
import com.sw.buyfriend.ui.adapter.AlternativeAdapter;
import com.sw.buyfriend.ui.adapter.NoteAdapter;

import java.util.ArrayList;

public class AlternativeFragment extends Fragment {

    private Product _currentProduct;
    private Brand _currentBrand;
    private ArrayList<Shop> _shops;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        return inflater.inflate(R.layout.activity_alternatives, container);
    }

    public void setCurrent(Brand brand,Product product){
        _currentProduct =product;
        _currentBrand=brand;

        _shops = new ArrayList<>();


        _shops.add(new Shop("L'aile du papillon","10, route de Nimes 34920 Le Cres",R.mipmap.l_aile_du_papillon,R.mipmap.l_aile_du_papillon));
        _shops.add(new Shop("Biocoop Marianne","181 Place Ernest Granier Immeuble Oz'One 34000 Montpellier",R.mipmap.biocoop_marianne,R.mipmap.biocoop_marianne));
        String t = "Saint-Gely-du-Fesc";
        _shops.add(new Shop("Verts de Terre","SARL Verts de Terre 152 rue du Pous 34980 Saint-Gely-du-Fesc",R.mipmap.verts_de_terre,R.mipmap.verts_de_terre));

        //ListView listView = (ListView) getActivity().findViewById(R.id.listAlternatives);

        //AlternativeAdapter noteAdapter = new AlternativeAdapter(getActivity(), _shops);
        //listView.setAdapter(noteAdapter);

        Button button = (Button) getActivity().findViewById(R.id.showAlternativesBtn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                ((MainActivity) v.getContext()).openMap();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

//    public void onResume() {
//        JSONObject json = null;
//        String productCode = "20147426";
//
//
//        currentProduct = Product.buildProductFromProductCode(productCode);
//        currentBrand = Brand.buildBrandFromProductCode(productCode);
//
//        TextView tv = (TextView) getActivity().findViewById(R.id.marqueTitle);
//        tv.setText(currentBrand.name);
//
//
//        ImageView logo = (ImageView) getActivity().findViewById(R.id.marqueThumb);
//        logo.setImageResource(currentBrand.thumbNail);
//    }
}
