package com.sw.buyfriend.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.sw.buyfriend.R;
import com.sw.buyfriend.pojo.Brand;
import com.sw.buyfriend.pojo.Product;
import com.sw.buyfriend.pojo.Shop;
import com.sw.buyfriend.ui.adapter.AlternativeAdapter;

import java.util.ArrayList;

public class MapFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        String[] values = new String[] { "La Boucherie de la Ferme",
                "Dalga Isabelle",
                "Folle Avoine"

        };

        ListView listView = (ListView) getActivity().findViewById(R.id.alternativeList);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, values);

        listView.setAdapter(adapter);

        Button myButton = (Button) getActivity().findViewById(R.id.showAlternativesBtn);
        myButton.setText("Nous avons 12 boutiques autour de vous qui proposent un produit similaire \nCliquez ici pour les afficher sur une carte");

        return inflater.inflate(R.layout.activity_details_map, container);
    }



//    public void onResume() {
//        JSONObject json = null;
//        String productCode = "20147426";
//
//
//        currentProduct = Product.buildProductFromProductCode(productCode);
//        currentBrand = Brand.buildBrandFromProductCode(productCode);
//
//        TextView tv = (TextView) getActivity().findViewById(R.id.marqueTitle);
//        tv.setText(currentBrand.name);
//
//
//        ImageView logo = (ImageView) getActivity().findViewById(R.id.marqueThumb);
//        logo.setImageResource(currentBrand.thumbNail);
//    }
}
