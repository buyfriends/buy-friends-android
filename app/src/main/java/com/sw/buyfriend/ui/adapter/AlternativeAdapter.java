package com.sw.buyfriend.ui.adapter;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sw.buyfriend.R;
import com.sw.buyfriend.pojo.Note;
import com.sw.buyfriend.pojo.Shop;

import java.util.List;

public class AlternativeAdapter extends BaseAdapter {

    private final Context mContext;
    List<Shop> shops;
    LayoutInflater inflater;

    public AlternativeAdapter(Context context, List<Shop> shops) {
        inflater = LayoutInflater.from(context);
        this.shops = shops;
        mContext = context;
    }

    public int getCount() {
        return shops.size();
    }

    public Object getItem(int position) {
        return shops.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View cv, ViewGroup parent) {
        ViewHolder holder;
        final Shop shop = shops.get(position);

        if (cv == null) {
            holder = new ViewHolder();
            cv = inflater.inflate(R.layout.adapter_shops, parent, false);
            holder.tvName = getTextView(R.id.tv_name, cv);
            holder.ivIcon = (ImageView) cv.findViewById(R.id.iv_icon);
            cv.setTag(holder);
        } else {
            holder = (ViewHolder) cv.getTag();
        }
        holder.tvName.setText(shop.name);
        holder.ivIcon.setImageResource(shop.thumbnail);
        Log.e(" ", holder.ivIcon.getDrawable().hashCode() + " ");
        return cv;
    }


    private TextView getTextView(int id, View cv) {
        return (TextView) cv.findViewById(id);
    }

    private class ViewHolder {
        TextView tvName;
        ImageView ivIcon;
    }
}