package com.sw.buyfriend.ui.adapter;


import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sw.buyfriend.R;
import com.sw.buyfriend.pojo.Note;

import java.util.List;

public class NoteAdapter extends BaseAdapter {

    private final Context mContext;
    List<Note> notes;
    LayoutInflater inflater;

    public NoteAdapter(Context context, List<Note> notes) {
        inflater = LayoutInflater.from(context);
        this.notes = notes;
        mContext = context;
    }

    public int getCount() {
        return notes.size();
    }

    public Object getItem(int position) {
        return notes.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View cv, ViewGroup parent) {
        ViewHolder holder;
        final Note note = notes.get(position);

        if (cv == null) {
            holder = new ViewHolder();
            cv = inflater.inflate(R.layout.adapter_notes, parent, false);
            holder.tvName = getTextView(R.id.tv_name, cv);
            holder.ivIcon = (ImageView) cv.findViewById(R.id.iv_icon);
            cv.setTag(holder);
        } else {
            holder = (ViewHolder) cv.getTag();
        }
        holder.tvName.setText(note.getSujet());
        holder.ivIcon.setImageResource(note.getImage());
        return cv;
    }


    private TextView getTextView(int id, View cv) {
        return (TextView) cv.findViewById(id);
    }

    private class ViewHolder {
        TextView tvName;
        ImageView ivIcon;
    }
}