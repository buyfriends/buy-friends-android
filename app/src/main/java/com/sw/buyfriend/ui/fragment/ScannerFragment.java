package com.sw.buyfriend.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.zxing.Result;
import com.sw.buyfriend.MainActivity;
import com.sw.buyfriend.ui.ImplBarcodeScannerView;

public class ScannerFragment extends Fragment implements ImplBarcodeScannerView.ResultHandler {
    private ImplBarcodeScannerView mScannerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mScannerView == null)
            mScannerView = new ImplBarcodeScannerView(getActivity());
        return mScannerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        Log.e("", rawResult.getText()); // Prints scan results
        Log.e("", rawResult.getBarcodeFormat().toString());

        ((MainActivity)this.getActivity()).openBrandDetails(rawResult.getBarcodeFormat().toString());
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }
}